private class MyWebViewClient extends WebViewClient {
  public boolean shouldOverrideUrlLoading(WebView view , String url){
       if (Uri.parse(url).getHost().equals("")) {
           return false;
       }
       Intent intent = new Intent(Intent.ACTION_VIEW , Uri.parse(url));
       startActivity(intent);
       return true;
   }
}
 
webView.setWebViewClient(new MyWebViewClient());

